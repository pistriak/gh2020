package com.gh.hw3.task2;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a number:");
        int n = scanner.nextInt();
        if (n < 0) {
            System.out.println("Good bye");
            return;
        }

        String[] words = new String[n];
        System.out.println("Enter words:");
        for (int i = 0; i < n; i++) {
            words[i] = scanner.next();
        }

        WordShortener wordShortener = new WordShortener();
        for (String word : words) {
            String message = String.format("Short version for '%s' is '%s'", word, wordShortener.makeShorter(word));
            System.out.println(message);
        }
    }
}
