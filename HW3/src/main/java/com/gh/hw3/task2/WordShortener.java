package com.gh.hw3.task2;

public class WordShortener {

    public String makeShorter(String word) {
        int length = word.length();
        if (length <= 10) {
            return word;
        }

        return "" + word.charAt(0) + (length - 2) + word.charAt(length - 1);
    }
}
