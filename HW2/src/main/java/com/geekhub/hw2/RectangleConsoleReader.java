package com.geekhub.hw2;

import java.util.Scanner;

public class RectangleConsoleReader {

    private final Scanner scanner;

    public RectangleConsoleReader(Scanner scanner) {
        this.scanner = scanner;
    }

    public Rectangle read() {
        System.out.println("Enter width:");
        double width = scanner.nextDouble();
        System.out.println("Enter length:");
        double length = scanner.nextDouble();

        return new Rectangle(width, length);
    }
}
