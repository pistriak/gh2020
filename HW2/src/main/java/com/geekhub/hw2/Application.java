package com.geekhub.hw2;

import java.util.Arrays;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("Choose one: " + Arrays.toString(ShapeType.values()));

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        ShapeType shapeType;
        try {
            shapeType = ShapeType.valueOf(input.trim().toUpperCase());
        } catch (Exception e) {
            System.out.println(input + " is wrong shape type");
            return;
        }

        Shape shape;
        switch (shapeType) {
            case RECTANGLE:
                RectangleConsoleReader rectangleConsoleReader = new RectangleConsoleReader(scanner);
                shape = rectangleConsoleReader.read();
                break;
            case SQUARE:
                SquareConsoleReader squareConsoleReader = new SquareConsoleReader(scanner);
                shape = squareConsoleReader.read();
                break;
            default:
                throw new RuntimeException(shapeType + " is not supported yet");
        }

        System.out.println("Shape perimeter = " + shape.calculatePerimeter());
        System.out.println("Shape area = " + shape.calculateArea());
    }
}
