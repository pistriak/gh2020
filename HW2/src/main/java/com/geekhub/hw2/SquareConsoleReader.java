package com.geekhub.hw2;

import java.util.Scanner;

public class SquareConsoleReader {

    private final Scanner scanner;

    public SquareConsoleReader(Scanner scanner) {
        this.scanner = scanner;
    }

    public Square read() {
        System.out.println("Enter square side:");
        double a = scanner.nextDouble();

        return new Square(a);
    }
}
