package com.geekhub.hw2;

public class Rectangle implements Shape {

    private final double width;
    private final double length;

    public Rectangle(double width, double length) {
        validate(width, length);
        this.width = width;
        this.length = length;
    }

    private void validate(double width, double length) {
        if (width <= 0 || length <= 0) {
            String message = String.format("Cannot create a rectangle with width = %f and length = %f", width, length);
            throw new IllegalArgumentException(message);
        }
    }

    @Override
    public double calculatePerimeter() {
        return 2 * (width + length);
    }

    @Override
    public double calculateArea() {
        return width * length;
    }
}
