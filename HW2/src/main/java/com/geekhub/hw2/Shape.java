package com.geekhub.hw2;

public interface Shape {
    double calculatePerimeter();

    double calculateArea();
}
