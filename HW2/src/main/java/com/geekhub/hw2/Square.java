package com.geekhub.hw2;

public class Square implements Shape {

    private final double a;

    public Square(double a) {
        validate(a);
        this.a = a;
    }

    private void validate(double a) {
        if (a <= 0) {
            throw new IllegalArgumentException("Cannot create a square with a = " + a);
        }
    }

    @Override
    public double calculatePerimeter() {
        return 4 * a;
    }

    @Override
    public double calculateArea() {
        return a * a;
    }
}