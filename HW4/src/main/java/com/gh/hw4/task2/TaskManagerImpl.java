package com.gh.hw4.task2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class TaskManagerImpl implements TaskManager {

    private final Map<LocalDateTime, List<Task>> tasksByDate = new TreeMap<>();

    @Override
    public void add(LocalDateTime date, Task task) {
        List<Task> tasks = tasksByDate.computeIfAbsent(date, k -> new ArrayList<>());
        tasks.add(task);
    }

    @Override
    public void remove(LocalDateTime date) {
        tasksByDate.remove(date);
    }

    @Override
    public Set<String> getCategories() {
        Set<String> categories = new HashSet<>();
        for (List<Task> tasks : tasksByDate.values()) {
            for (Task task : tasks) {
                categories.add(task.getCategory());
            }
        }
        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories(String... categories) {
        Map<String, List<Task>> tasksByCategory = new HashMap<>();

        Set<String> categorySet = new HashSet<>(Arrays.asList(categories));

        //using a tree map - we don't need to sort keys because they are sorted already.

        for (List<Task> tasks : tasksByDate.values()) {
            for (Task task : tasks) {
                if (categorySet.contains(task.getCategory())) {
                    List<Task> categoryTasks = tasksByCategory.computeIfAbsent(task.getCategory(), k -> new ArrayList<>());
                    categoryTasks.add(task);
                }
            }
        }

        return tasksByCategory;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        Map<String, List<Task>> tasksByCategories = getTasksByCategories(category); // we have reused the method above
        List<Task> categoryTasks = tasksByCategories.get(category);
        return categoryTasks != null ? categoryTasks : Collections.emptyList();
    }

    @Override
    public List<Task> getTasksForToday() {
        LocalDate today = LocalDate.now();

        List<Task> todayTasks = new ArrayList<>();
        for (Map.Entry<LocalDateTime, List<Task>> dateAndTasks : tasksByDate.entrySet()) {
            LocalDateTime date = dateAndTasks.getKey();
            if (today.equals(date.toLocalDate())) {
                List<Task> tasks = dateAndTasks.getValue();
                todayTasks.addAll(tasks);
            }
        }

        return todayTasks;
    }
}
